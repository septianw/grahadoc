mysql -u root -pubuntu -h db < /database.sql
cp /tmp/.htaccess /var/www/.htaccess
chown -R www-data:www-data /var/www
find /var/www -type d -exec chmod 500 {} \;
find /var/www -type f -exec chmod 400 {} \;
find /var/www/wp-content/uploads -type d -exec chmod 700 {} \;
find /var/www/wp-content/uploads -type f -exec chmod 600 {} \;
