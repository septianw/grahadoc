<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'ubuntu');

/** MySQL hostname */
define('DB_HOST', 'db');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'bHCzTyt;ZrnZ;{DGkf>2Vej_?IE%auRVC*jijXVG&x}HwX1?_>V-FQg/y|:9AF(8');
define('SECURE_AUTH_KEY',  'T$cF|$X{6_q[/oB&%]{s=<EHx}M=>`Xj!!~;GLyEOktm?Z#DIcYH5~l 9;bwtjs-');
define('LOGGED_IN_KEY',    'G!Ct&Ez-)}TJr99_PP]o6uSRHiE3IOmWud=9yZ2QfM.O%Q;Z$>),F|4ipvlIfL&z');
define('NONCE_KEY',        '}Wgc~C!GP=p$Ekz?2Y2oU}kMoN=5N~2N=#vWlYu.)MbzFU!$Mmam7!eETeJP5k@W');
define('AUTH_SALT',        'aZhIgkmRC*f6:TX(vQ5F`fRtSc_MF0h3/uMR#W|lZK+g)#C#Z8Q^aiZFf@UR:+k.');
define('SECURE_AUTH_SALT', '$Jc1_r j;n%.,Uk# p#cH1qzou#qT=BeKEhjh%7PNctIJ=n=Bz4QVXn7>&45:G-3');
define('LOGGED_IN_SALT',   '-Iayz^.L`p2cN3r@-5,%~gYN<4>$+`{&q4L]{7g*y$OV}c4XIw`=G{Aekq!yAnzb');
define('NONCE_SALT',       '=N{^vTu/=Bl^<T(f:e_bo!=x(10oek|.uRJqaEi=1p+jQ2EO)_<>KTJ#/8,3FC^3');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'growl_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
