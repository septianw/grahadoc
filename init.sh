#!/bin/bash
set -e

baseconf=/etc/aranea/wordpress;

getname() {
#  curl -s http://www.whimsicalwordimal.com/api/name | jshon -e name -u | tr " " - | tr '[:upper:]' '[:lower:]'
  names | tr " " - | tr '[:upper:]' '[:lower:]'

}

spawndisk () {

  local nama=$1
  size=$2
  out=$3
  in=$4

  if [ -z $4 ]
  then
#    echo "Deploying disk";

    cat /dev/zero | dd bs=4K count=$[((1024*1024)/4096)*size] > $out$nama.img 2>&1
    dev=$(sudo losetup -f)
    sudo losetup $dev $out$nama.img
    sudo mkfs.ext4 $dev >/dev/null 2>/dev/null
    sudo losetup -d $dev
    disk=$out$nama.img
    echo $disk
  else
#    echo "Deploying disk"
    size=$(du -d 0 $in | cut -f1);
    cat /dev/zero | dd bs=4K count=$[(((size*1024)/4096)*2)+1] > $out$nama.img 2>&1
    dev=$(sudo losetup -f)
    sudo losetup $dev $out$nama.img
    sudo mkfs.ext4 $dev >/dev/null 2>/dev/null
    sudo losetup -d $dev
    disk=$out$nama.img
    echo $disk
  fi
}

mondisk () {
  local name=$1;
  case "$2" in
  "share")
    if [ ! -d $(pwd)/space ]
    then
      mkdir $(pwd)/space
    fi
    spawndisk $name $3 $(pwd)/
    sudo mount $(pwd)/$name.img $(pwd)/space
    ;;
  "code")
    if [ ! -d $(pwd)/code ]
    then
      mkdir $(pwd)/code;
    fi
    spawndisk $name 0 $(pwd)/ $3
    webcodepath=$(cat araneafile | grep webcodepath | cut -d"=" -f2);
    sudo mount $(pwd)/$name.img $(pwd)/$webcodepath
    ;;
  esac
}

clone () {
  git clone $1 $(pwd)/tmp
  echo $(pwd)/tmp
}

acquire_config () {
  local basedir=$(pwd)
  local webdocker=$basedir/web/Dockerfile
  local sshdocker=$basedir/ssh/Dockerfile
  cp $baseconf/docker-compose-base docker-compose.yml

  for couple in `cat araneafile`
  do
    key=$(echo $couple | cut -d"=" -f1)
    value=$(echo $couple | cut -d"=" -f2)
    sed -i -e 's_'"$key"'_'"$value"'_g' docker-compose.yml
  done
  
  sed -i -e 's_mysqldbdata_'"$1-dbdata"'_g' docker-compose.yml

  if [ -f $webdocker ]
  then
    rm $webdocker
  fi

  if [ -f $sshdocker ]
  then
    rm $sshdocker
  fi

  cp $baseconf/webbasedockfile $webdocker
  cp $baseconf/sshbasedockfile $sshdocker

  sed -i -e '/^FROM/r '"$(pwd)/buildwebhook" $(pwd)/web/Dockerfile
  sed -i -e '/^FROM/r '"$(pwd)/buildsshhook" $(pwd)/ssh/Dockerfile
}

runafterdeploy () {
  local cid=$(docker ps | grep ${PWD##*/}_web | cut -d" " -f1);
  docker exec $cid /first-run/afterdeploy.sh
}

if [ -f .lock ]
then
  echo 'Site already inited.';
  exit 1;
fi

name=$(getname);
echo $name > site;
namespace=$name"-space";
namecode=$name"-code";
disksize=$(cat araneafile | grep webdisklimit | cut -d"=" -f2);
repo=$(cat araneafile | grep repo | cut -d"=" -f2);
webcodepath=$(cat araneafile | grep webcodepath | cut -d"=" -f2);

locsite=$(cat araneafile | grep locsite | cut -d"=" -f2);
remsite=$(cat araneafile | grep remsite | cut -d"=" -f2);

docker create --name $name-dbdata -v /var/lib/mysql mariadb

if [ -f $(pwd)/web/database.sql ]
then
  sed -i -e 's_'"$locsite"'_'"$remsite"'_g' $(pwd)/web/database.sql
fi

mondisk $namespace "share" $disksize
tmpd=$(clone $repo)
mondisk $namecode "code" $tmpd
mv $tmpd/{.[!.],}* $(pwd)/$webcodepath
rm -rf $tmpd

acquire_config $name
docker-compose up -d

runafterdeploy

touch .lock
